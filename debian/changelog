usb-permissions (2025.1) unstable; urgency=medium

  * Updated for 2025.1

 -- Development Team <devteam@openthinclient.com>  Tue, 04 Feb 2025 23:21:14 +0100

usb-permissions (2022.1) unstable; urgency=medium

  * Version bump

 -- Development Team <devteam@openthinclient.com>  Wed, Sep 14 2022 16:33:34 +0200

usb-permissions (2021.2) unstable; urgency=medium

  * Fixed non-matching USB-IDs with leading zeros
  * Disabled old license logic
  * Reordered and renamed options, added hints

 -- Development Team <devteam@openthinclient.com>  Mon, 29 Mar 2021 16:41:12 +0100

usb-permissions (2020.2) unstable; urgency=medium

  * Recompiled for 64bit
  * Some fixes to be compatible to Debian Buster

 -- Development Team <devteam@openthinclient.com>  Thu, 05 Nov 2020 14:54:34 +0200

usb-permissions (2019.1) unstable; urgency=low

  * TCOS-balloon does not work on debian 9 from udev-rule, replaced with notify-send
  * Fixed udev rule to work with debian 9

 -- Development Team <devteam@openthinclient.com>  Wed, 31 Jul 2019 14:23:29 +0200

usb-permissions (2018.1) unstable; urgency=low

  * Support of REST API was added
  * Fix for usb devices without DRIVER field

 -- Vladyslav Savchenko <v.savchenko@openthinclient.com>  Mon, 10 Sep 2018 16:00:00 +0200

usb-permissions (2.1-2.0.4) unstable; urgency=low

  * Fix for devices without SERIAL
  * Fix for hubs

 -- Vladyslav Savchenko <v.savchenko@openthinclient.com>  Fri, 19 Jan 2018 12:51:00 +0200

usb-permissions (2.1-2.0.3) unstable; urgency=low

  * Fix for high loading on boot or restart of udev system

 -- Vladyslav Savchenko <v.savchenko@openthinclient.com>  Fri, 29 Sep 2017 15:36:00 +0200

usb-permissions (2.1-2.0.2) unstable; urgency=low

  * Fix for verbosity (commands in logs)

 -- Vladyslav Savchenko <v.savchenko@openthinclient.com>  Mon, 14 Aug 2017 16:12:00 +0200

usb-permissions (2.1-2.0.1) unstable; urgency=low

  * Fix for device enabling/disabling

 -- Vladyslav Savchenko <v.savchenko@openthinclient.com>  Wed, 26 Jul 2017 18:57:00 +0200

usb-permissions (2.1-2) unstable; urgency=low

  * Order of Options was changed
  * Fix for remote windows-sessions

 -- Vladyslav Savchenko <v.savchenko@openthinclient.com>  Thu, 23 Mar 2017 11:51:00 +0200

usb-permissions (2.1-1.2.0) unstable; urgency=low

  * Allowance/denial only for usb-storages was added

 -- Vladyslav Savchenko <v.savchenko@openthinclient.com>  Tue, 21 Mar 2017 11:45:00 +0200

usb-permissions (2.1-1.1.0) unstable; urgency=low

  * Error message was replaced with a popup notification
  * Allowance/denial by serial number was added

 -- Vladyslav Savchenko <v.savchenko@openthinclient.com>  Thu, 16 Mar 2017 17:33:00 +0200

usb-permissions (2.1-1.0.0) unstable; urgency=low

  * Initial release

 -- Vladyslav Savchenko <v.savchenko@openthinclient.com>  Wed, 08 Mar 2017 17:04:00 +0200
