device-denied-title = USB Gerät nicht erlaubt
device-denied-message = { $DEVICE_NAME }

    USB-ID:		{ $PRODUCT }
    Serien-Nr.:	{ $SERIAL }

    { $REASON ->
        [all]         USB-Geräte sind deaktiviert
        [usbhid]      USB-Eingabegeräte sind deaktiviert
        [usb-storage] USB-Speichergeräte sind deaktiviert
        [specific]    Dieses spezifische USB-Gerät ist deaktiviert
        *[else] {""}
    }
