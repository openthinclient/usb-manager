device-denied-title = USB device not allowed
device-denied-message = { $DEVICE_NAME }

    USB-ID:		{ $PRODUCT }
    Serial No.:	{ $SERIAL }

    { $REASON ->
        [all]         USB devices are disabled
        [usbhid]      USB input devices are disabled
        [usb-storage] USB storage devices are disabled
        [specific]    This specific USB device is disabled
        *[else] {""}
    }
